#!/bin/bash
cd .. &&
cd api-gateway/igg-api && yarn && cd -
cd microservices/igg-auth && yarn && cd -
cd microservices/igg-mail && yarn && cd -
cd microservices/igg-booking && yarn && cd -
cd microservices/igg-customer && yarn && cd -
cd microservices/igg-catalog && yarn && cd -
cd microservices/igg-payment && yarn && cd -
cd ml/igg-ml && yarn && cd -
cd ml/igg-ml/client && yarn && cd -
cd frontends/vue-otasoft && yarn && cd -
cd frontends/react-otasoft && yarn && cd -
