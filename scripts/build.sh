cd .. &&
cd api-gateway/igg-api && yarn build && cd -
cd microservices/igg-auth && yarn build && cd -
cd microservices/igg-mail && yarn build && cd -
cd microservices/igg-booking && yarn build && cd -
cd microservices/igg-customer && yarn build && cd -
cd microservices/igg-catalog && yarn build && cd -
cd microservices/igg-payment && yarn build && cd -
cd ml/igg-ml && yarn build && cd -
cd ml/igg-ml/client && yarn build && cd -
cd frontends/vue-otasoft && yarn build && cd -
cd frontends/react-otasoft && yarn build && cd -
